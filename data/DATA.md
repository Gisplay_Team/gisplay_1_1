# Datasets

Inside data.zip there are four datasets.

1. ```usa_accidents_by_county.json``` - Contains accidents by county.

2. ```usa_accidents_by_county_centroid.json``` - Contains accidents by county as a centroid.

3. ```usa_accidents_by_year_state``` - For each state has the number of accidents and related information between 2009 and 2013.

4. ```usa_accidents``` - Accidents  on USA organized by points. Each accident is one point. 

This datasets were pre-processed based on the data available at [National Highway Traffic Safety Administration(nhtsa)](ftp://ftp.nhtsa.dot.gov/fars/).